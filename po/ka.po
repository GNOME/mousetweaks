# Georgian translation for mousetweaks.
# Copyright (C) 2023 mousetweaks's COPYRIGHT HOLDER
# This file is distributed under the same license as the mousetweaks package.
# Ekaterine Papava <papava.e@gtu.ge>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: mousetweaks master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/mousetweaks/"
"issues\n"
"POT-Creation-Date: 2023-05-29 12:36+0000\n"
"PO-Revision-Date: 2023-06-15 05:02+0200\n"
"Last-Translator: Ekaterine Papava <papava.e@gtu.ge>\n"
"Language-Team: Georgian <ka@li.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.3.1\n"

#: data/mousetweaks.ui:8
msgid "Hover Click"
msgstr "გადატარება წკაპი"

#: data/mousetweaks.ui:56
msgid "Single Click"
msgstr "ერთი წკაპი"

#: data/mousetweaks.ui:111
msgid "Double Click"
msgstr "ორმაგი წკაპი"

#. 'Drag' like in a Drag and Drop operation
#: data/mousetweaks.ui:166
msgid "Drag"
msgstr "გადათრევა"

#: data/mousetweaks.ui:221
msgid "Secondary Click"
msgstr "დამატებით წკაპი"

#: data/mousetweaks.ui:249
msgid "Button Style"
msgstr "ღილაკის სტილი"

#: data/mousetweaks.ui:257
msgid "Text only"
msgstr "მხოლოდ ტექსტი"

#: data/mousetweaks.ui:267
msgid "Icons only"
msgstr "მხოლოდ ხატულები"

#: data/mousetweaks.ui:277
msgid "Text and Icons"
msgstr "ტექსტი და ხატულები"

#: data/mousetweaks.ui:287
msgid "Orientation"
msgstr "ორიენტაცია"

#. Buttons are arranged from left to right in the layout
#: data/mousetweaks.ui:295
msgid "Horizontal"
msgstr "ჰორიზონტალური"

#. Buttons are arranged from top to bottom in the layout
#: data/mousetweaks.ui:305
msgid "Vertical"
msgstr "ვერტიკალური"

#: data/org.gnome.mousetweaks.gschema.xml:5
msgid "Click-type window style"
msgstr "დაწკაპუნების ტიპის ფანჯრის სტილი"

#: data/org.gnome.mousetweaks.gschema.xml:6
msgid "Button style of the click-type window."
msgstr "დაწკაპუნების ტიპის ფანჯრის ღილაკის სტილი."

#: data/org.gnome.mousetweaks.gschema.xml:10
msgid "Click-type window orientation"
msgstr "დაწკაპუნების ტიპის ფანჯრის ორიენტაცია"

#: data/org.gnome.mousetweaks.gschema.xml:11
msgid "Orientation of the click-type window."
msgstr "დაწკაპუნების ტიპის ფანჯრის ორიენტაცია."

#: data/org.gnome.mousetweaks.gschema.xml:15 src/mt-main.c:638
msgid "Click-type window geometry"
msgstr "დაწკაპუნების ტიპის ფანჯრის გეომეტრია"

#: data/org.gnome.mousetweaks.gschema.xml:16
msgid ""
"Size and position of the click-type window. The format is a standard X "
"Window System geometry string."
msgstr ""

#: src/mt-main.c:624
msgid "Enable dwell click"
msgstr ""

#: src/mt-main.c:626
msgid "Enable simulated secondary click"
msgstr ""

#: src/mt-main.c:628
msgid "Time to wait before a dwell click"
msgstr ""

#: src/mt-main.c:630
msgid "Time to wait before a simulated secondary click"
msgstr ""

#: src/mt-main.c:632
msgid "Set the active dwell mode"
msgstr ""

#: src/mt-main.c:634
msgid "Hide the click-type window"
msgstr "დაწკაპუნების ტიპის ფანჯრის დამალვა"

#: src/mt-main.c:636
msgid "Ignore small pointer movements"
msgstr "კურსორის მცირე მოძრაობის იგნორი"

#: src/mt-main.c:640
msgid "Shut down mousetweaks"
msgstr "Mousetweaks-ის გამორთვა"

#: src/mt-main.c:642
msgid "Start mousetweaks as a daemon"
msgstr "Mousetweaks-ის დემონის სახით გაშვება"

#: src/mt-main.c:644
msgid "Start mousetweaks in login mode"
msgstr "Mousetweaks-ის შესვლის რეჟიმში გაშვება"

#. parse
#: src/mt-main.c:662
msgid "- GNOME mouse accessibility daemon"
msgstr ""

#: src/mt-main.c:814
msgid "Shutdown failed or nothing to shut down.\n"
msgstr "გამორთვა შეუძლებელია ან გამოსართავი არაფერია.\n"

#: src/mt-main.c:816
msgid "Shutdown successful.\n"
msgstr "გამორთვა წარმატებულია.\n"

#. i18n: PID here means "Process Identifier"
#: src/mt-main.c:824
#, c-format
msgid "Mousetweaks is already running. (PID %u)\n"
msgstr "Mousetweaks უკვე გაშვებულია. (PID %u)\n"

#: src/mt-main.c:830
msgid "Starting daemon.\n"
msgstr "დემონის გაშვება.\n"

#: src/mt-common.c:97
msgid "Failed to Display Help"
msgstr "დახმარების ჩვენების შეცდომა"
